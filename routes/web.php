<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@login');
Route::post('/', 'LoginController@login');
Route::get('/login', 'LoginController@login');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

Route::get('/users', 'UserController@list');
Route::get('/user', 'UserController@new');
Route::post('/user', 'UserController@new');
Route::get('/user/{id}', 'UserController@update');
Route::post('/user/{id}', 'UserController@update');
Route::get('/user/remove/{id}', 'UserController@remove');

Route::get('/dashboard', 'ProductController@list');
Route::get('/product', 'ProductController@new');
Route::post('/product', 'ProductController@new');
Route::get('/product/{id}', 'ProductController@update');
Route::post('/product/{id}', 'ProductController@update');
Route::get('/product/remove/{id}', 'ProductController@remove');

Route::get('/categorys','CategoryController@list');
Route::get('/category', 'CategoryController@new');
Route::post('/category', 'CategoryController@new');
Route::get('/category/{id}', 'CategoryController@update');
Route::post('/category/{id}', 'CategoryController@update');
Route::get('/category/remove/{id}', 'CategoryController@remove');

Route::get('/profiles', 'ProfileController@list');
Route::get('/profile', 'ProfileController@new');
Route::post('/profile', 'ProfileController@new');
Route::get('/profile/{id}', 'ProfileController@update');
Route::post('/profile/{id}', 'ProfileController@update');
Route::get('/profile/remove/{id}', 'ProfileController@remove');