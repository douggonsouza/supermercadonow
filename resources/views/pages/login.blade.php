@extends('nowLogin')

@section('content')

    <div class="row">
        <div class="col-sd-3" style="margin-left: auto; margin-right: auto; padding: 64px">
            <div class="card text-white bg-dark mb-3">
                <h5 class="card-header">Login</h5>
                <div class="card-body">
            
                    <form action="" method="post">
                        @csrf <!-- {{ csrf_field() }} -->
                        <div class="form-group">
                            <label for="exampleInputEmail1">Usuário</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Seu e-mail...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-primary">Entrar</button>
                    </form>
                </div>
            </div>
            <small id="emailHelp" class="form-text text-muted" style="text-align: center">Cadastrar-se.</small>
        </div>
    </div>

@endsection