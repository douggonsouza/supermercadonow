@extends('now')

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header">Usuário</h5>
                <div class="card-body">
            
                    <form action="" method="post">
                        @csrf <!-- {{ csrf_field() }} -->
                        <input type="hidden" name="user_id" class="form-control" value="<?= isset($user)? $user['user_id']: null; ?>">
                        <div class="form-group">
                            <label for="exampleInputProfile">* Perfil:</label>
                            <?php
                                echo Form::select(
                                'profile_id',
                                $profiles,
                                isset($user)? $user['profile_id']: null,
                                [
                                'class'    => 'form-control',
                                'id'       => 'exampleInputProfile',
                                'required' => 'required'
                                ]
                            );
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName">* Nome:</label>
                            <input type="text" name="name" class="form-control" id="exampleInputName" value="<?= isset($user)? $user['name']: null; ?>" aria-describedby="emailHelp" placeholder="" required="required">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail">* E-mail:</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail" value="<?= isset($user)? $user['email']: null; ?>" aria-describedby="emailHelp" placeholder="" required="required">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword">* Senha:</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword" placeholder="" required="required">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
            
                </div>
            </div>
        </div>
    </div>
</div>

@endsection