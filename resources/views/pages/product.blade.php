@extends('now')

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header">Produto</h5>
                <div class="card-body">
            
                    <form action="" method="post" enctype="multipart/form-data">
                        @csrf <!-- {{ csrf_field() }} -->
                        <input type="hidden" name="product_id" class="form-control" value="<?= isset($product)? $product['product_id']: null; ?>">
                        <div class="form-group">
                            <label for="exampleInputCategory">* Categoria:</label>
                            <?php
                                echo Form::select(
                                'category_id',
                                $listCategorys,
                                isset($product)? $product['category_id']: null,
                                [
                                'class'    => 'form-control',
                                'id'       => 'exampleInputCategory',
                                'required' => 'required'
                                ]
                            );
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName">* Nome:</label>
                            <input type="text" name="name" class="form-control" id="exampleInputName" value="<?= isset($product)? $product['name']: null; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputValue">* Valor:</label>
                            <input type="text" name="value" class="form-control" id="exampleInputValue" value="<?= isset($product)? number_format((float) $product['value'],2,',',''): null; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputStatus">* Status:</label>
                            <?php
                                echo Form::select(
                                'status',
                                $listStatus,
                                isset($product)? $product['status']: null,
                                [
                                'class'    => 'form-control',
                                'id'       => 'exampleInputCategory',
                                'required' => 'required'
                                ]
                            );
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputImagem">* Imagem:</label>
                            <input type="file" name="image" class="form-control" id="exampleInputImagem">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputDescription">Textarea:</label>
                            <textarea name="description" class="form-control" id="exampleInputDescription" rows="3"><?= isset($product)? trim($product['description']): null; ?></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
            
                </div>
            </div>
        </div>
    </div>
</div>

@endsection