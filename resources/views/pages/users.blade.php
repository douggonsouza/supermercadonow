@extends('now')

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header">Usuários</h5>
                <div class="card-body">
                    <div class="col-sd-12">
                        <div class="col-sd-3">
                            <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/user'; ?>"><button type="submit" class="btn btn-primary">Novo</button></a>
                        </div>
                    </div>
                    <table
                        id="table"
                        data-toggle="table"
                        data-search="true"
                        data-custom-search="customSearch"
                        data-search-text=""
                        data-pagination="true"
                        data-show-columns="true">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(isset($users) || !empty($users)){
                                        foreach($users as $item){
                                ?>
                                            <tr id="tr-id-1" class="tr-class-1" data-title="bootstrap table" data-object='{"key": "value"}'>
                                                <td id="td-id-1" class="td-class-1" data-title="bootstrap table">
                                                <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/user/'.$item['user_id']; ?>"><?= $item['user_id']; ?></a>
                                                </td>
                                                <td data-value="526">
                                                <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/user/'.$item['user_id']; ?>"><?= $item['name']; ?></a>
                                                </td>
                                                <td data-text="122"><?= $item['email']; ?></td>
                                                <td style="text-align: center">
                                                    <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/user/remove/'.$item['user_id']; ?>"><button type="button" class="btn btn-danger">Deletar</button></a>
                                                </td>
                                            </tr>
                                <?php
                                        }
                                        $dataMessage = null;
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $('#dtBasicExample').DataTable();
        $('.dataTables_length').addClass('bs-select');
    });

</script>

@endsection