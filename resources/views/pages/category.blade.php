@extends('now')

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header">Categoria</h5>
                <div class="card-body">
            
                    <form action="" method="post">
                        @csrf <!-- {{ csrf_field() }} -->
                        <input type="hidden" name="category_id" value="<?= isset($category)? $category['category_id']: null; ?>" class="form-control" >
                        <div class="form-group">
                            <label for="exampleInputName">* Nome:</label>
                            <input type="text" name="name" class="form-control" id="exampleInputName" value="<?= isset($category)? $category['name']: null; ?>" aria-describedby="emailHelp" required="required">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputDescription">Textarea:</label>
                            <textarea name="description" class="form-control" id="exampleInputDescription" rows="3"><?= isset($category)? $category['description']: null; ?></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
            
                </div>
            </div>
        </div>
    </div>
</div>

@endsection