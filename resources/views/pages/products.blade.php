@extends('now')

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header">Produtos</h5>
                <div class="card-body">
                    <div class="col-sd-12" style="margin-bottom: 21px;">
                            <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/product'; ?>"><button type="button" class="btn btn-primary">Novo</button></a>
                    </div>
                    <div class="col-sd-12">
                        <h6 class="card-header">Filtros</h6>
                        <form action="" method="get">
                            @csrf <!-- {{ csrf_field() }} -->
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputName">Name</label>
                                    <input type="text" name="name" class="form-control" id="inputName">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputCategory">Category</label>
                                    <?php
                                        echo Form::select(
                                        'category_id',
                                        array_merge([ null => 'Select...'], $listCategorys),
                                        null,
                                        [
                                        'class'    => 'form-control',
                                        'id'       => 'inputCategory'
                                        ]
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputStatus">Status</label>
                                    <?php
                                        echo Form::select(
                                        'status',
                                        array_merge([ null => 'Select...'], $listStatus),
                                        null,
                                        [
                                        'class'    => 'form-control',
                                        'id'       => 'inputStatus'
                                        ]
                                    );
                                    ?>
                                </div>
                            </div>
                            <button type="submit" id="inputSubmit" class="btn btn-primary">Filtrar</button>
                        </form>
                    </div>
                    <table
                        id="table"
                        data-toggle="table"
                        data-search="true"
                        data-custom-search="customSearch"
                        data-search-text=""
                        data-pagination="true"
                        data-show-columns="true">
                            <thead>
                                <tr>
                                    <th style="text-align: center">ID</th>
                                    <th style="text-align: center">Name</th>
                                    <th style="text-align: center">Category</th>
                                    <th style="text-align: center">Status</th>
                                    <th style="text-align: center">Value</th>
                                    <th style="text-align: center">Description</th>
                                    <th style="text-align: center">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(isset($products) || !empty($products)){
                                        foreach($products as $item){
                                ?>
                                            <tr id="tr-id-1" class="tr-class-1" data-title="bootstrap table" data-object='{"key": "value"}'>
                                                <td style="text-align: center" id="td-id-1" class="td-class-1" data-title="bootstrap table">
                                                    <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/product/'.$item['product_id']; ?>"><?= $item['product_id']; ?></a>
                                                </td>
                                                <td style="text-align: center" data-value="526"><a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/product/'.$item['product_id']; ?>"><?= $item['name']; ?></a></td>
                                                <td style="text-align: center" data-text="122"><?= $listCategorys[(int) $item['category_id']]; ?></td>
                                                <td style="text-align: center" data-text="25"><?= $listStatus[$item['status']]; ?></td>
                                                <td style="text-align: center" data-value="526,99"><?= number_format((float) $item['value'],2,',',''); ?></td>
                                                <td style="text-align: center" data-i18n="Description"><?= $item['description']; ?></td>
                                                <td style="text-align: center">
                                                    <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/product/remove/'.$item['product_id']; ?>"><button type="button" class="btn btn-danger">Deletar</button></a>
                                                </td>
                                            </tr>
                                <?php
                                        }
                                        $dataMessage = null;
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $('#dtBasicExample').DataTable();
        $('.dataTables_length').addClass('bs-select');
    });

</script>

@endsection