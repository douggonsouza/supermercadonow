<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">NOW</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#textoNavbar" aria-controls="textoNavbar" aria-expanded="false" aria-label="Alterna navegação">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="textoNavbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/index'; ?>">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/logout'; ?>">Sair</a>
      </li>
    </ul>
    <span class="navbar-text">
      <?= isset($userName)? $userName: null; ?>
    </span>
  </div>
</nav>
  