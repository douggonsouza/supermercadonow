<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const STATUS_PENDING = 'pending';
    const STATUS_ANALYSING = 'analysing';
    const STATUS_APPROVED = 'approved';
    const STATUS_REPROVED = 'reproved';

    const STATUS_LIST = [
        self::STATUS_PENDING => 'Pendente',
        self::STATUS_ANALYSING => 'Em análise',
        self::STATUS_APPROVED => 'Aprovado',
        self::STATUS_REPROVED => 'Reprovado'
    ];

    const STATUS_LIST_DEFAULT = [
        self::STATUS_PENDING => 'Pendente',
        self::STATUS_ANALYSING => 'Em análise',
    ];
    /**
     * Nome da tabela
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Chave primária
     *
     * @var string
     */
    protected $primaryKey = 'product_id';

    /**
     * Existencia de created_at e updated_at
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'name', 'value', 'description', 'image', 'status',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['product_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at'];

    /**
     * Get the phone record associated with the user.
     */
    public function category()
    {
        return $this->hasOne('App\Category', 'category_id');
    }

    /**
     * Lista de categorias
     *
     * @return void
     */
    public static function list()
    {
        return self::all()->pluck('product_id', 'name')->toArra();
    }
}
