<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use App\Http\Controllers\Alerts;
use App\User;

class LoginController extends Controller
{
    /**
     * Logar usuário
     *
     * @return void
     */
    public function login()
    {
        if(array_key_exists('email',$_POST) || array_key_exists('password',$_POST)){
            $user = $this->in($_POST['email'], $_POST['password']);
            if($user){
                // guardar usuário na sessão
                session('login.user', $user->toArray()[0]);
                Alerts::set('Usuário logado com sucesso');
                return redirect()->action('ProductController@list');
            }
            Alerts::set('E-mail ou senha inválido.',Alerts::ERROR);
        }
        return view('pages.login');
    }

    private function in($email, $password)
    {
        $user = User::where([
            'email'    => $email,
            'password' => md5($password), 
        ])->get();

        return count($user) > 0? $user: false;
    }

    /**
     * Limpa log da sessão
     *
     * @return void
     */
    public function logout()
    {
        $request = new Request();
        if($request->hasSession()){
            $request->session()->forget('login.user');
        }
        Alerts::set('A sessão do usuário foi limpa com sucesso.');
        
        return redirect()->action('loginController@login');
    }

    /**
     * Verifica se existe usuário logado
     *
     * @return boolean
     */
    public static function isLoged()
    {
        return Session::has('login.user');
    }
}