<?php

namespace App\Http\Controllers;

abstract class Alerts
{

    const SUCCESS = 'success';
    const ERROR   = 'error';
    const WARNING = 'warning';
    const INFO    = 'info';

    /**
     * Busca pela mensagem de alerta na sessão
     * 
     * 
     * @return bool
     */
    static final function searchInSession()
    {
        if(!isset($_SESSION['msgAlert']) || empty($_SESSION['msgAlert'])){
            return false;
        }
        return $_SESSION['msgAlert']['msgs'];
    }

    /**
     * Salva na sessão a mensagem de alerta
     * 
     * @return array
     */
    public static final function saveInSession(string $alert)
    {
        if(!isset($alert) || empty($alert)){
            return false;
        }

        $_SESSION['msgAlert']['msgs'][] = $alert;
        return true;
    }

    /**
     * Devolve o conteúdo do alerta
     * 
     * @param string $mensagem
     * @param string $type
     * 
     * @return string
     */
    static final public function set($mensagem,$type = 'success')
    {        
        // testa o conteúdo da variável
        if(isset($mensagem) && strlen($mensagem) > 0){
            switch($type){
                case self::SUCCESS:                        
                    $alert = sprintf('<div class="alert alert-%s" role="alert">%s</div>', 'success', 'Sucesso: '.$mensagem);
                    break;
                case self::ERROR:  
                    $alert = sprintf('<div class="alert alert-%s" role="alert">%s</div>', 'danger', 'Perigo: '.$mensagem);                      
                    break;
                case self::WARNING:
                    $alert = sprintf('<div class="alert alert-%s" role="alert">%s</div>', 'warning', 'Cuidado: '.$mensagem);                       
                    break;                    
                case self::INFO:
                    $alert = sprintf('<div class="alert alert-%s" role="alert">%s</div>', 'info', 'Informação: '.$mensagem);                        
                    break;
                default: 
                    $alert = sprintf('<div class="alert alert-%s" role="alert">%s</div>', 'success', 'Sucesso: '.$mensagem);                                              
            }
        }

        self::saveInSession($alert);

        return true;
    }

    /**
     * Retorna alerta definido
     * 
     * @param string $clear
     * 
     * @return mixed
     */
    final static public function get($clear = true)
    {
        $alerts = self::searchInSession();
                        
        if(!isset($alerts) || empty($alerts))
            return null;

        if($clear) $_SESSION['msgAlert']['msgs'] = [];
        return implode("\n ",$alerts);
    }

    /**
     * Limpa a mensagem de alerta
     */
    final public function clear()
    {
        self::$alerta = [];
        return self;
    }

    /**
     * Get the value of exists
     */ 
    final static public function exist()
    {
        if(isset($_SESSION['msgAlert']) && !empty($_SESSION['msgAlert'])){
            if(!empty($_SESSION['msgAlert']['msgs']))
                return true;
            return false;
        }

        return false;
    }
}

    