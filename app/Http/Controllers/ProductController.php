<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Alerts;
use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    private $dirUploadImage;

    public function __construct(){
        $this->dirUploadImage = $_SERVER['DOCUMENT_ROOT'].$_ENV['LOCAL_IMAGES'].'/';
        // $request = session('login.user');
        // if(!$request->hasSession()){
        //     header('location:'.'http://'.$_SERVER['SERVER_NAME'].'/');
        // }
    }
    /**
     * Listagem de produtos
     *
     * @return void
     */
    public function list()
    {
        $listCategorys = array_map('utf8_decode', Category::list());
        $listCategorys = array_map('utf8_encode', $listCategorys);
        $listStatus = Product::STATUS_LIST;

        $products = new Product();
        //filtrar
        if(isset($_GET['_token']) && !empty($_POST['_token'])){
            if(isset($_GET['name']) && !empty($_GET['name'])){
                $products = $products->where('name', 'like', '%'.$_GET['name'].'%');;
            }
            if(isset($_GET['category_id']) && !empty($_GET['category_id'])){
                $products = $products->where('category_id', $_GET['category_id']);
            }
            if(isset($_GET['status']) && !empty($_GET['status'])){
                $products = $products->where('status', $_GET['status']);
            }
        }

        $products = $products->get()->toArray();
        return view('pages.products', [
            'products'   => $products,
            'listStatus' => $listStatus,
            'listCategorys' => !empty($listCategorys)? $listCategorys: []
        ]);
    }

    /**
     * Inicia novo produto
     *
     * @return void
     */
    public function new()
    {
        $request = new Request();
        $profileId = null;
        if($request->session()->has('login.user'))
            $profileId = Session::get('login.user')['profile_id'];
        $listCategorys = array_map('utf8_decode', Category::list());
        $listCategorys = array_map('utf8_encode', $listCategorys);
        $listStatus    = Product::STATUS_LIST_DEFAULT;
        if(isset($profileId) && $profileId == 1)
            $listStatus = Product::STATUS_LIST;

        if(isset($_POST['_token']) && !empty($_POST['_token'])){
            // salve imagem
            
            $img = $this->saveImage($_FILES['image']);
            if(!isset($img)){
                Alerts::set('Erro no salvamento da imagem.');
                return view('pages.product',[
                    'dirUploadImage' => $this->dirUploadImage,
                    'listStatus'    => $listStatus,
                    'product'       => null,
                    'listCategorys' => !empty($listCategorys)? $listCategorys: []
                ]);
            }
            
            $product = new Product();
            $product->category_id = $_POST['category_id'];
            $product->name   = $_POST['name'];
            $product->value  = number_format((float) $_POST['value'],2,'.','');
            $product->status = $_POST['status'];
            $product->image  = $img;
            $product->description = !empty($_POST['description'])? $_POST['description']: null;
            if($product->save()){
                Alerts::set('Produto salvo com sucesso.');
                return view('pages.product',[
                    'dirUploadImage' => $this->dirUploadImage,
                    'listStatus'    => $listStatus,
                    'product'       => null,
                    'listCategorys' => !empty($listCategorys)? $listCategorys: []
                ]);
            }
            Alerts::set('Erro ao salvar Produto.');
        }

        return view('pages.product',[
            'dirUploadImage' => $this->dirUploadImage,
            'listStatus'    => $listStatus,
            'product'       => null,
            'listCategorys' => !empty($listCategorys)? $listCategorys: []
        ]);
    }
    
    /**
     * Atualiza o produto
     *
     * @param int $id
     * @return void
     */
    public function update($id)
    {
        $product = Product::find($id);
        if(empty($product->toArray())){
            Alerts::set('Erro no carregamento do Produto.','error');
            return redirect()->action('ProductController@list');
        }

        // $request = new Request();
        // $profileId = null;
        // if($request->session()->hasSession()){
        //     $profileId = session('login.user')['profile_id'];
        // }
        // $listStatus    = Product::STATUS_LIST_DEFAULT;
        // if(isset($profileId) && $profileId == 1)
        $listStatus = Product::STATUS_LIST;
        $listCategorys = array_map('utf8_decode', Category::list());
        $listCategorys = array_map('utf8_encode', $listCategorys);

        if(isset($_POST['_token']) && $_POST['_token'] == 'Dhjidwg5hRqA9OimYd2P6NmYGjyGsfofI7fd8qrY'){
            // salve imagem
            $img = null;
            if(isset($_FILES) && !empty($_FILES['image'])){
                $img = $this->saveImage($_FILES['image']);
            }
            if(!isset($img)){
                Alerts::set('Erro no salvamento da imagem.');
                return redirect()->action('ProductController@list');
            }

            if(!empty($_POST['name']))
                $product->name   = $_POST['name'];
            if(!empty($_POST['value']))
                $product->value  = number_format((float) $_POST['value'],2,'.','');
            if(!empty($_POST['status']))
                $product->status = $_POST['status'];
            if(isset($img))
                $product->image  = $img;
            if(!empty($_POST['description']))
                $product->description = $_POST['description'];
            if($product->save()){
                Alerts::set('Produto salvo com sucesso.');
                return redirect()->action('ProductController@list');
            }
        }

        return view('pages.product',[
            'dirUploadImage' => $this->dirUploadImage,
            'listStatus'     => $listStatus,
            'product'        => $product->toArray(),
            'listCategorys'  => !empty($listCategorys)? $listCategorys: []
        ]);

    }

    private function saveImage($file)
    {
        if(!isset($file) || empty($file)){
            return null;
        }

        if ($file["error"] > 0){
            return null;
        }

        if(!move_uploaded_file($file["tmp_name"], $this->dirUploadImage.$file["name"])){
            return null;
        }

        return $file["name"];
    }

    /**
     * Deleção lógica do produto
     *
     * @param int $id
     * @return void
     */
    public function remove($id)
    {
        $product = Product::find($id);
        if(!empty($product->toArray())){
            $product->delete();
            return $this->list();
        }
        Alerts::set('Erro na deleção do Produto.');
        return $this->list();
    }
}