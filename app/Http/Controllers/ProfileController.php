<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Profile;

class ProfileController extends Controller
{
    public function __construct()
    {
        // $request = new Request();
        // if(!$request->hasSession()){
        //     header('location:'.'http://'.$_SERVER['SERVER_NAME'].'/');
        // }
    }

    /**
     * Listagem de usuárioss
     *
     * @return void
     */
    public function list()
    {
        $profiles = Profile::all()->toArray();
        return view('pages.profiles', [ 'profiles' => $profiles ]);
    }

    /**
     * Inicia novo perfil
     *
     * @return void
     */
    public function new()
    {
        if(isset($_POST['_token']) && !empty($_POST['_token'])){            
            $profile = new Profile();
            $profile->name        = $_POST['name'];
            $profile->description = $_POST['description'];
            if($profile->save()){
                Alerts::set('Categoria salva com sucesso.');
                return view('pages.profile');
            }
            Alerts::set('Erro ao salvar Produto.');
        }

        return view('pages.profile');
    }
    
    /**
     * Atualiza o usuário
     *
     * @param int $id
     * @return void
     */
    public function update($id)
    {
        $profile = Profile::find($id);
        if(empty($profile->toArray())){
            Alerts::set('Erro no carregamento da Categoria.','error');
            return view('pages.profile', [
                'profile' => $profile->toArray()
            ]);
        }

        if(isset($_POST['_token']) && !empty($_POST['_token'])){
            if(!empty($_POST['name']))
                $profile->name        = $_POST['name'];
            if(!empty($_POST['description']))
                $profile->description = $_POST['description'];
            if($profile->save()){
                Alerts::set('Usuário salvo com sucesso.');
                return view('pages.profile', [
                    'profile' => $profile->toArray()
                ]);
            }
        }

        return view('pages.profile', [
            'profile' => $profile->toArray()
        ]);
    }

    /**
     * Deleção lógica do usuário
     *
     * @param int $id
     * @return void
     */
    public function remove($id){
        $profile = Profile::find($id);
        if(!empty($profile->toArray())){
            $profile->delete();
            Alerts::set('Produto deletado com sucesso.');
            return $this->list();
        }
        Alerts::set('Erro na deleção do Produto.');
        return $this->list();
    }
}