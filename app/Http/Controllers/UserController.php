<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Profile;

class UserController extends Controller
{
    public function __construct()
    {
        // $request = new Request();
        // if(!$request->hasSession()){
        //     header('location:'.'http://'.$_SERVER['SERVER_NAME'].'/');
        // }
    }

    /**
     * Listagem de usuárioss
     *
     * @return void
     */
    public function list()
    {
        $profiles = Profile::list();
        $users = User::all()->toArray();
        return view('pages.users', [
            'users' => $users,
            'profiles' => $profiles
            ]);
    }

    /**
     * Inicia novo usuário.
     *
     * @return void
     */
    public function new()
    {
        $profiles = Profile::list();

        if(isset($_POST['_token']) && !empty($_POST['_token'])){            
            $user = new User();
            $user->profile_id = $_POST['profile_id'];
            $user->name     = $_POST['name'];
            $user->email    = $_POST['email'];
            $user->password = md5($_POST['password']);
            if($user->save()){
                Alerts::set('Categoria salva com sucesso.');
                return view('pages.user', [
                    'profiles' => $profiles
                ]);
            }
            Alerts::set('Erro ao salvar Produto.');
        }

        return view('pages.user', [
            'profiles' => $profiles
        ]);
    }

    /**
     * Atualiza o usuário
     *
     * @param int $id
     * @return void
     */
    public function update($id)
    {
        $user = User::find($id);
        if(empty($user->toArray())){
            Alerts::set('Erro no carregamento da Categoria.','error');
            return view('pages.user', [
                'profiles' => $profiles,
                'user' => null
            ]);
        }

        $profiles = Profile::list();

        if(isset($_POST['_token']) && !empty($_POST['_token'])){
            if(!empty($_POST['name']))
                $user->name     = $_POST['name'];
            if(!empty($_POST['email']))
                $user->email    = $_POST['email'];
            if(!empty($_POST['password']))
                $user->password = md5($_POST['password']);
            if($user->save()){
                Alerts::set('Usuário salvo com sucesso.');
                return view('pages.user', [
                    'profiles' => $profiles,
                    'user' => $user->toArray()
                ]);
            }
        }

        return view('pages.user', [
            'profiles' => $profiles,
            'user' => $user->toArray()
        ]);
    }

    /**
     * Deleção lógica do usuário
     *
     * @param int $id
     * @return void
     */
    public function remove($id){
        $user = Category::find($id);
        if(!empty($user->toArray())){
            $user->delete();
            return $this->list();
        }
        Alerts::set('Erro na deleção do Produto.');
        return $this->list();
    }
}