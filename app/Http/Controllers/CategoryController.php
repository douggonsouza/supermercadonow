<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        // $request = new Request();
        // if(!$request->hasSession()){
        //     header('location:'.'http://'.$_SERVER['SERVER_NAME'].'/');
        // }
    }
    /**
     * Listagem de categorias
     *
     * @return void
     */
    public function list()
    {
        $categorys = Category::all()->toArray();
        return view('pages.categorys', [ 'categorys' => $categorys ]);
    }

    /**
     * Inicia novo produto
     *
     * @return void
     */
    public function new()
    {
        if(isset($_POST['_token']) && !empty($_POST['_token'])){            
            $category = new Category();
            $category->name   = $_POST['name'];
            $category->description = !empty($_POST['description'])? $_POST['description']: null;
            if($category->save()){
                Alerts::set('Categoria salva com sucesso.');
                return view('pages.category');
            }
            Alerts::set('Erro ao salvar Produto.');
        }

        return view('pages.category');
    }
    
    /**
     * Atualiza o produto
     *
     * @param int $id
     * @return void
     */
    public function update($id)
    {
        $category = Category::find($id);
        if(empty($category->toArray())){
            Alerts::set('Erro no carregamento da Categoria.','error');
            return view('pages.category', [ 'category' => $category->toArray() ]);
        }

        if(isset($_POST['_token']) && !empty($_POST['_token'])){
            if(!empty($_POST['name']))
                $category->name        = $_POST['name'];
            if(!empty($_POST['description']))
                $category->description = $_POST['description'];
            if($category->save()){
                Alerts::set('Categoria salva com sucesso.');
                return view('pages.category', [ 'category' => $category->toArray() ]);
            }
        }

        return view('pages.category', [ 'category' => $category->toArray() ]);
    }

    /**
     * Deleção lógica do produto
     *
     * @param int $id
     * @return void
     */
    public function remove($id)
    {
        $category = Category::find($id);
        if(!empty($category->toArray())){
            $category->delete();
            Alerts::set('Produto deletado com sucesso.');
            return $this->list();
        }
        Alerts::set('Erro na deleção do Produto.');
        return $this->list();
    }
}