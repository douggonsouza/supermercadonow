<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * Nome da tabela
     *
     * @var string
     */
    protected $table = 'profiles';

    /**
     * Chave primária
     *
     * @var string
     */
    protected $primaryKey = 'profile_id';

    /**
     * Existencia de created_at e updated_at
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['profile_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
    ];

    /**
     * Lista de categorias
     *
     * @return void
     */
    public static function list()
    {
        return self::all()->pluck('name','profile_id')->toArray();
    }
}
