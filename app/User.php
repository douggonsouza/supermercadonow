<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Nome da tabela
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Chave primária
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * Existencia de created_at e updated_at
     *
     * @var boolean
     */
    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_id', 'name', 'email', 'password',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['user_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function profile()
    {
        return $this->hasOne('App\Profile');
    }
}
