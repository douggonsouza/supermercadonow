<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'profile_id' => 1,
            'name'       => 'Administrador',
            'email'      => 'douggonsouza@gmail.com',
            'password'   => MD5('Mudar@123'),
        ]);
    }
}
