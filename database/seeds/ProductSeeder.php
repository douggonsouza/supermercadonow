<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'category_id' => 1,
            'name'        => 'Vestuário',
            'value'       => 59.99,
            'status'      => Product::STATUS_PENDING,
            'image'       => '30489.png',
            'description' => 'Primeiro produto',
        ]);
    }
}
