<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'name'        => 'Administrador',
            'description' => 'Perfil de administrador',
        ]);

        DB::table('profiles')->insert([
            'name'        => 'Padrão',
            'description' => 'Perfil padrão',
        ]);
    }
}
